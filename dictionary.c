#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// TODO
	// Allocate memory for an array of strings (arr).
	char **strArr = (char **)malloc(5000 * sizeof(*strArr));
	
	// Read the dictionary line by line.
   
	// Expand array if necessary (realloc).
	
	// Allocate memory for the string (str).
	int counter = 0;
	char *str = (char *)malloc(50 * sizeof(char*));
    while(fgets(str, 50, in)) {
    	char *nl = strchr(str, '\n');
		if (nl) *nl = '\0';
        
        strArr[counter] = (char *)malloc(50 * sizeof(char*));
        strcpy(strArr[counter], str);
        
        counter++;
    }
	// Copy each line into the string (use strcpy).
	// Attach the string to the large array (assignment =).
	
	
	// The size should be the number of entries in the array.
	*size = counter;
	
	// Return pointer to the array of strings.
	return strArr;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.

char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}